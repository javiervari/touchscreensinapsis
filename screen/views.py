
import time, json, os

from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings

from .aux import scan, Mqtt, connect_wifi

BROKER = '192.168.1.100'
TOPIC = 'vikua/sinapsis/ta/device'

def get_coord(cliente_mqtt):
    """Retorna un objeto de tipo:
        {'lat':X, 'lng':Y, 'status': True} en caso de GPS DISPONIBLE
        o
        {'status': False} en caso de GPS NO DISPONIBLE
    """
    if cliente_mqtt.connected == False:
        cliente_mqtt = Mqtt(broker=BROKER, topic='#')

    cliente_mqtt.publish_message(topico=TOPIC, msg='coord')
    time.sleep(1)
    
    coordinates = cliente_mqtt.coord['coordinates']
    if coordinates != False:
        coordinates = coordinates.split(",")
        data = {'lat':float(coordinates[0]), 'lng':float(coordinates[1]), 'status':1}
    else:
        data = {'status':0}
    return data

def get_info(cliente_mqtt):
    try:
            
        if cliente_mqtt.connected == False: #deberia poner un try/except para las conexiones mqtt
            cliente_mqtt = Mqtt(broker=BROKER, topic='#')
        cliente_mqtt.publish_message(topico=TOPIC, msg='info')
        time.sleep(1)
        informacion = cliente_mqtt.informacion


    except Exception as e:
        informacion = False #OJO

    finally:
        return informacion




@csrf_exempt 
def connect_view(request):
    if request.method=='GET':
        
        networks = False
        while networks == False:
            try:
                networks = scan()
            except Exception as e:
                networks = False
                time.sleep(5)

        contexto = {'networks':networks}

        return render(request,'networks.html', contexto)
    else:
        wifi = request.POST['wifi']
        status = connect_wifi(ssid=wifi, password=str(settings.WIFI_PASSWORD))

        return redirect('screen:menu_view')





def menu_view(request):
    global cliente_mqtt

    cliente_mqtt = Mqtt(broker=BROKER, topic='#') #Cambiar por direccion ip por host con el scanner
    return render(request,'menu.html', {})



@csrf_exempt 
def map_view(request):
    data = get_coord(cliente_mqtt)

    if request.method=='GET':
        return render(request, 'map.html', data)

    elif request.method=='POST': #AJAX
        return HttpResponse(json.dumps(data,cls=DjangoJSONEncoder))




def info_view(request):
    try:
        info = get_info(cliente_mqtt)
    except NameError :
        cliente_mqtt = Mqtt(broker=BROKER, topic='#')
        info = get_info(cliente_mqtt)



    contexto = {'contexto':info}
    return render(request,'informacion.html', contexto)

