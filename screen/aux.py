import time, json, os
from subprocess import check_output

import paho.mqtt.client as mqttc
import wifi
import gpsd
from cryptography.fernet import Fernet


KEY = "UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE="
CIPHER_SUITE = Fernet(KEY)


# def scan(): VIEJO
#     WiFi = []
#     cmd = 'sudo iwlist wlan0 scan'.split()
#     scanoutput = check_output(cmd)
#     ssid = 'WiFi not found'

#     for line in scanoutput.split():
#         line = line.decode('utf-8')
#         if line[:5]=='ESSID':
#             ssid=line.split(":")[1]
#             WiFi.append(ssid.replace('"',''))
#     return WiFi[0:10]

def available_wifi_networks():
    """Devuelve en un objeto map TODAS las redes wifi disponibles"""
    cells = wifi.Cell.all('wlan0')
    return cells



def storaged_wifi_networks(ssid):
    """Devuelve en un objeto map todas las redes almacenadas al menos una vez"""
    cell = wifi.Scheme.find('wlan0', ssid)
    if cell:
        return cell
    return False



def scan():
    """Devuelve una lista de las 5 primeras redes wifi"""
    WiFi = []
    cells = available_wifi_networks()
    for cell in cells:
        WiFi.append(cell.ssid)
    
    return WiFi[0:10]



def connect_wifi(ssid, password):
    file = '/etc/wpa_supplicant/wpa_supplicant.conf'
    text = open(file).read()
    print("SSID es", ssid)

    if ssid in text:
        try:
            os.system('sudo wpa_supplicant -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0 &')
            time.sleep(5)
            return True
        except Exception as e:
            print("ERROR",e)
            return False

        
    else:
        network_conf = """
            ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
            update_config=1
            country=VE

            network="""

        with open(file, 'w') as f:
            print('editar')
            body = """
                    ssid="{}"
                    psk="{}"
                    key_mgmt=WPA-PSK
            """.format(ssid, password)

            network_conf = network_conf+'{'+body+'}'
            f.write(network_conf)
            f.close()

        try:
            os.system('sudo wpa_supplicant -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0 &')
            time.sleep(5)
            os.system("sudo reboot now")
            return True
        except Exception as e:
            print("ERROR",e)
            return False
            




class Mqtt():
    """
        Clase que crea el cliente mqtt y gestiona los servicios
        Pametros obligatorios: broker y puerto
        Parametros no obligatorios: topico

        Retorna: cliente MQTT
    """
    def __init__(self, broker, topic):
        """
            Constructor, crea atributos de la clase e inicia la conexion con el broker
        """
        gpsd.connect()

        self.connected = False
        self.broker = broker #'localhost'
        self.topic = topic #'#'
        self.port = 1883

        self.client = mqttc.Client()
        self.client_connection()


    def on_message(self, client, userdata, message):
        message = CIPHER_SUITE.decrypt(message.payload)
        message = json.loads(message.decode('utf-8'))

        if 'sinapsis_id' in message:
            self.informacion = message

        elif 'coordinates' in message:
            self.coord = message
            

        
    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.connected = True
            self.client.subscribe(self.topic, 1)
            print('connected')
        else:
            self.connected = False

    def on_disconnect(self, client, userdata, flags, rc):
        self.connected = False
        self.client_connection()

    def publish_message(self, msg, topico):
        try:
            status = self.client.publish(topico, msg, 1)
            return status
        except Exception as e:
            print('error',e)
            return False

    def client_connection(self):
        """
            Inicia la conexion con el broker. 
            Si la clase recibe un topico entones se realiza el callback ON_MESSAGE, este metodo esta destinado a los Sinapsis de conteo
            Se realizan los callbacks de ON_CONNECT y ON_DISCONNECT
            Se intenta hacer la conexion 
        """
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect

        try:
            self.client.connect(self.broker, self.port, 60)
            self.client.loop_start()
            time.sleep(2)
            # self.client.loop_stop()
        except Exception as e:
            print("[MQTT]:",e)


if __name__ == "__main__":
    cliente = Mqtt()

           
