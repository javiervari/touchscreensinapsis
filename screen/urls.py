from django.urls import path

from screen.views import menu_view, map_view, connect_view, info_view, connect_view

app_name = 'screen'

urlpatterns = [
    # path('', connect_view, name='connect_view'),
    # path('', connect_view, name='connect_view'),
    path('', connect_view, name='connect_view'),
    path('menu', menu_view, name='menu_view'),
    path('map', map_view, name='map_view'),
    path('info', info_view, name='info_view'),


]