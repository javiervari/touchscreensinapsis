

$( document ).ready(function() {

    var coord = {lat: lat, lng: lng} //Declaradas en el HTML

    var mymap = L.map('mapid').setView(coord, zoom);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);
    
    var mark = L.marker(coord).addTo(mymap)
    var popup = L.popup();
    
    setInterval(function(){
        
        $.ajax({
            url: 'map',
            data:[],
            dataType: 'json',
            type:'POST',
           
            success: function (data) {
                
                if (data.status == 1){
                    const coord = {lat: data.lat, lng: data.lng}
                    
                    mymap.zoomIn(18)
                    mymap.panTo(coord)

                    mark.setLatLng(coord)


                }else if(data.status == 0){
                    console.log("BUSCANDO GPS...")
                }
                
            }
        })
        
        
    }, 1000);
    
    
    
    
});



















  
  
